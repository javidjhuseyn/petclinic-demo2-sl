def call(){
    def execCmd = 'bash ./compose-commands.sh'
    sshagent(['ec2-petclinic']) {
        sh "scp -o StrictHostKeyChecking=no compose-commands.sh ec2-user@35.180.47.10:/home/ec2-user"
        sh "scp -o StrictHostKeyChecking=no docker-compose.yaml ec2-user@35.180.47.10:/home/ec2-user"
        sh "ssh -o StrictHostKeyChecking=no ec2-user@35.180.47.10 uptime ${execCmd}"
    }
}

return this
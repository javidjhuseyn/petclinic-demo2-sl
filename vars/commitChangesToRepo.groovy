def call(){
    echo 'Commiting version changes to the Git repository ...'
    withCredentials([usernamePassword(credentialsId: 'gitlab-credentials', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]){
        sh 'git config --global user.email "jenkins@jenkins.com"'
        sh 'git config --global user.name "jenkins-admin"'
        
        sh "git remote set-url origin https://${USERNAME}:XRMzuwsftAuqmqkzuonR@gitlab.com/javidjhuseyn/petclinic-demo2-main.git"

        sh 'git add .'
        sh 'git commit -m "Update pom.xml" . '
        sh 'git push origin HEAD:master'
    }
}
return this
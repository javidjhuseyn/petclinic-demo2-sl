def call(){
    echo 'Building Docker image ...'
    withCredentials([usernamePassword(credentialsId: 'dockerhub-credentials', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]){
        sh "docker build -t javidjhuseyn/petclinic-demo:${IMAGE_NAME} ."
        sh "echo ${PASSWORD} | docker login -u ${USERNAME} --password-stdin"
    }
}

return this